import React from 'react';
import ComponentCreator from '@docusaurus/ComponentCreator';

export default [
  {
    path: '/tp4Devops/__docusaurus/debug',
    component: ComponentCreator('/tp4Devops/__docusaurus/debug', 'fb2'),
    exact: true
  },
  {
    path: '/tp4Devops/__docusaurus/debug/config',
    component: ComponentCreator('/tp4Devops/__docusaurus/debug/config', 'bb4'),
    exact: true
  },
  {
    path: '/tp4Devops/__docusaurus/debug/content',
    component: ComponentCreator('/tp4Devops/__docusaurus/debug/content', '3a4'),
    exact: true
  },
  {
    path: '/tp4Devops/__docusaurus/debug/globalData',
    component: ComponentCreator('/tp4Devops/__docusaurus/debug/globalData', '941'),
    exact: true
  },
  {
    path: '/tp4Devops/__docusaurus/debug/metadata',
    component: ComponentCreator('/tp4Devops/__docusaurus/debug/metadata', '8f1'),
    exact: true
  },
  {
    path: '/tp4Devops/__docusaurus/debug/registry',
    component: ComponentCreator('/tp4Devops/__docusaurus/debug/registry', '60d'),
    exact: true
  },
  {
    path: '/tp4Devops/__docusaurus/debug/routes',
    component: ComponentCreator('/tp4Devops/__docusaurus/debug/routes', 'a76'),
    exact: true
  },
  {
    path: '/tp4Devops/blog',
    component: ComponentCreator('/tp4Devops/blog', 'e14'),
    exact: true
  },
  {
    path: '/tp4Devops/blog/archive',
    component: ComponentCreator('/tp4Devops/blog/archive', 'e13'),
    exact: true
  },
  {
    path: '/tp4Devops/blog/first-blog-post',
    component: ComponentCreator('/tp4Devops/blog/first-blog-post', 'f78'),
    exact: true
  },
  {
    path: '/tp4Devops/blog/long-blog-post',
    component: ComponentCreator('/tp4Devops/blog/long-blog-post', '4ee'),
    exact: true
  },
  {
    path: '/tp4Devops/blog/mdx-blog-post',
    component: ComponentCreator('/tp4Devops/blog/mdx-blog-post', 'c0c'),
    exact: true
  },
  {
    path: '/tp4Devops/blog/tags',
    component: ComponentCreator('/tp4Devops/blog/tags', '213'),
    exact: true
  },
  {
    path: '/tp4Devops/blog/tags/docusaurus',
    component: ComponentCreator('/tp4Devops/blog/tags/docusaurus', '78f'),
    exact: true
  },
  {
    path: '/tp4Devops/blog/tags/facebook',
    component: ComponentCreator('/tp4Devops/blog/tags/facebook', 'd1c'),
    exact: true
  },
  {
    path: '/tp4Devops/blog/tags/hello',
    component: ComponentCreator('/tp4Devops/blog/tags/hello', '2cc'),
    exact: true
  },
  {
    path: '/tp4Devops/blog/tags/hola',
    component: ComponentCreator('/tp4Devops/blog/tags/hola', '72c'),
    exact: true
  },
  {
    path: '/tp4Devops/blog/welcome',
    component: ComponentCreator('/tp4Devops/blog/welcome', '0b7'),
    exact: true
  },
  {
    path: '/tp4Devops/markdown-page',
    component: ComponentCreator('/tp4Devops/markdown-page', '2dd'),
    exact: true
  },
  {
    path: '/tp4Devops/docs',
    component: ComponentCreator('/tp4Devops/docs', '05c'),
    routes: [
      {
        path: '/tp4Devops/docs',
        component: ComponentCreator('/tp4Devops/docs', '9ab'),
        routes: [
          {
            path: '/tp4Devops/docs',
            component: ComponentCreator('/tp4Devops/docs', '742'),
            routes: [
              {
                path: '/tp4Devops/docs/category/tutorial---basics',
                component: ComponentCreator('/tp4Devops/docs/category/tutorial---basics', 'a06'),
                exact: true,
                sidebar: "tutorialSidebar"
              },
              {
                path: '/tp4Devops/docs/category/tutorial---extras',
                component: ComponentCreator('/tp4Devops/docs/category/tutorial---extras', '109'),
                exact: true,
                sidebar: "tutorialSidebar"
              },
              {
                path: '/tp4Devops/docs/intro',
                component: ComponentCreator('/tp4Devops/docs/intro', 'e64'),
                exact: true,
                sidebar: "tutorialSidebar"
              },
              {
                path: '/tp4Devops/docs/tutorial-basics/congratulations',
                component: ComponentCreator('/tp4Devops/docs/tutorial-basics/congratulations', '966'),
                exact: true,
                sidebar: "tutorialSidebar"
              },
              {
                path: '/tp4Devops/docs/tutorial-basics/create-a-blog-post',
                component: ComponentCreator('/tp4Devops/docs/tutorial-basics/create-a-blog-post', '6af'),
                exact: true,
                sidebar: "tutorialSidebar"
              },
              {
                path: '/tp4Devops/docs/tutorial-basics/create-a-document',
                component: ComponentCreator('/tp4Devops/docs/tutorial-basics/create-a-document', 'b83'),
                exact: true,
                sidebar: "tutorialSidebar"
              },
              {
                path: '/tp4Devops/docs/tutorial-basics/create-a-page',
                component: ComponentCreator('/tp4Devops/docs/tutorial-basics/create-a-page', 'a89'),
                exact: true,
                sidebar: "tutorialSidebar"
              },
              {
                path: '/tp4Devops/docs/tutorial-basics/deploy-your-site',
                component: ComponentCreator('/tp4Devops/docs/tutorial-basics/deploy-your-site', '5a1'),
                exact: true,
                sidebar: "tutorialSidebar"
              },
              {
                path: '/tp4Devops/docs/tutorial-basics/markdown-features',
                component: ComponentCreator('/tp4Devops/docs/tutorial-basics/markdown-features', '1f5'),
                exact: true,
                sidebar: "tutorialSidebar"
              },
              {
                path: '/tp4Devops/docs/tutorial-extras/manage-docs-versions',
                component: ComponentCreator('/tp4Devops/docs/tutorial-extras/manage-docs-versions', 'e47'),
                exact: true,
                sidebar: "tutorialSidebar"
              },
              {
                path: '/tp4Devops/docs/tutorial-extras/translate-your-site',
                component: ComponentCreator('/tp4Devops/docs/tutorial-extras/translate-your-site', '516'),
                exact: true,
                sidebar: "tutorialSidebar"
              }
            ]
          }
        ]
      }
    ]
  },
  {
    path: '/tp4Devops/',
    component: ComponentCreator('/tp4Devops/', '8ff'),
    exact: true
  },
  {
    path: '*',
    component: ComponentCreator('*'),
  },
];
